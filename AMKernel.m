%% These MATLAB files correspond to a video from Th@MathThing's YouTube channel at http://www.thatmaththing/

%If you use these files for a project, please cite the following video: https://youtu.be/inHe3_DZL9g
%Also cite Dr. Joel A. Rosenfeld and include the website http://www.thelearningdock.org


tic
% Time domain
    t = 0:0.001:3;

% Carrier Signals
    yc1 = sin(2*pi*100*t);
    
    yc2 = sin(2*pi*150*t);
    
    yc3 = sin(2*pi*200*t);

% Plot Carrier Signals
    figure
    hold on
    plot(t,yc1);
    plot(t,yc2);
    plot(t,yc3);
    hold off
    
% Modulating Signal
    signal1 = 10*sin(2*pi*10*t) + 5*sin(2*pi*4*t) - 15*sin(2*pi*t);
    
    signal2 = 4*sin(2*pi*5*t) - 10*cos(2*pi*10*t) - 3*sin(2*pi*3*t);
    
    signal3 = 14*cos(2*pi*2*t) - 3*cos(2*pi*10*t) + 7*sin(2*pi*15*t);
    
% Plot Carrier Signals
    figure
    hold on
    plot(t,signal1);
    plot(t,signal2);
    plot(t,signal3);
    hold off
    
% Modulated Signal
    mody1 = (1 + signal1/30).*yc1;
    mody2 = (1 + signal2/30).*yc2;
    mody3 = (1 + signal3/30).*yc3;
    
    fullsum = mody1 + mody2 + mody3;
    
% Plot Carrier Signals
    figure
    hold on
    plot(t,mody1);
    plot(t,mody2);
    plot(t,mody3);
    hold off
    
% Kernel Function
    CarrierFrequency = 150;
    FrequencyWidth = 15;
    K = @(tt,s) sin(2*pi*(CarrierFrequency-FrequencyWidth:CarrierFrequency+FrequencyWidth)*tt)*sin(2*pi*(CarrierFrequency-FrequencyWidth:CarrierFrequency+FrequencyWidth)*s)' + cos(2*pi*(CarrierFrequency-FrequencyWidth:CarrierFrequency+FrequencyWidth)*tt)*cos(2*pi*(CarrierFrequency-FrequencyWidth:CarrierFrequency+FrequencyWidth)*s)';
    
% Integrating Against the Kernel (Normalization Doesn't Matter Here)

    Integrating = zeros(1,length(t));
    
    for i = 1:length(t)
        for j = 1:length(t)
            Integrating(i) = Integrating(i) + fullsum(j)*K(t(j),t(i));
        end
        Integrating(i) = Integrating(i)/(length(t)/3);
    end
    
    figure
    plot(t,mody2);
    title('Modulated Carrier Signal');

    figure
    plot(t,Integrating);
    title('Projected Signal');
    
    figure
    plot(t,signal2);
    title('Original Signal');

    figure
    plot(t,fullsum);
    title('Full Sum of Modulated Carrier Signals');
    
    
% FFT Stuff
fs = 1000;
L = length(t);
f = fs/L*(-L/2:L/2-1);
FullSignalFourierTransform = fftshift(fft(fullsum));
FourierTransform = fftshift(fft(Integrating));

RecoveryFFT = fftshift(fft(Integrating.*yc2));

RecoveryFFT = RecoveryFFT.*(abs(f) < 20);


figure
plot(f,FourierTransform);
figure
plot(f,FullSignalFourierTransform);

figure
plot(t,Integrating.*yc2);
figure
plot(t,real(ifft(ifftshift(RecoveryFFT))));





toc